FROM node:latest

COPY ./specs /specs
RUN apt-get update && apt-get install -y xvfb xauth wkhtmltopdf && chmod -R 777 /specs