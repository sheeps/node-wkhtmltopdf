#/bin/bash

node -v \
	&& npm -v \
	&& xvfb-run --auto-servernum --server-num=1 wkhtmltopdf http://www.google.com ./google.pdf \
	&& [ -e ./google.pdf ]